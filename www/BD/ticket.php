<?php
  function departementAll(){
    return ["Informatique", "Chimie", "GEA", "GMP", "GTE", "QLIO"];
  }
  function salleAll($Departement){
    return ["I-01", "I-02", "I-03", "I-04", "I-05", "I-06", "I-07", "I-11", "I-12", "I-21", "I-22", "I-23"];
  }
  function typeAll($salle, $Departement){
    return ["ordinateur", "fenetre", "table", "video projecteur", "tableaux", "chaise"];
  }
  function ordinateurAll($salle, $Departement){
    return ["info04-21", "info04-22", "info04-23", "info04-24", "info04-25", "info04-26"];
  }
  function sourceAll($type){
    return ["tour", "clavier", "souris", "ecran"];
  }
  function anomalieAll($type, $anomalie){
    return ["ne s'allume pas", "contient windows", "le cd ne veux pas sortir", "est un pc de l'IUT"];
  }

?>
