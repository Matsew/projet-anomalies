#!/bin/bash
# Crée les fichiers manquants
cd Symfony
composer i
npm install bootstrap
npm run build
echo "DATABASE_URL=sqlite3:///db.db3" > .env
# echo "APP_ENV=prod" >> .env
echo "Utilisez la commande suivante pour lancer l'application :"
echo "symfony server:start --no-tls"