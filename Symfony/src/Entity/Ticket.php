<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Ticket
{
    protected $email;
    protected $departement;
    protected $salle;
    protected $types;
    protected $nom;
    protected $source;
    protected $problem;
    protected $description;
    protected $picture;


    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getDepartement(): string
    {
        return $this->departement;
    }

    public function setDepartement(string $departement): void
    {
        $this->departement = $departement;
    }

    public function getSalle(): string
    {
        return $this->salle;
    }

    public function setSalle(string $salle): void
    {
        $this->salle = $salle;
    }

    public function getTypes(): string
    {
        return $this->types;
    }

    public function setTypes(string $types): void
    {
        $this->types = $types;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }


    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    public function getProblem(): string
    {
        return $this->problem;
    }

    public function setProblem(string $problem): void
    {
        $this->problem = $problem;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function toQuery(): array
    {
        return [
            //"name" => $this->getName(),
            "content" => $this->getDescription(),
            "status" => 1,
            "_users_id_requester" => 0,
            "_users_id_requester_notif" => [
                $this->getEmail()
            ],
            // itilcategories_id
        ];
    }
}