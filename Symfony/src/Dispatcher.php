<?php


namespace App;



use ProjetAnomalies\GlpiApi\ClientDispatcher;


class Dispatcher
{
    private static ?ClientDispatcher $INSTANCE = null;

    /**
     * @throws \Exception
     */
    public static function getInstance(): ClientDispatcher
    {
        if (Dispatcher::$INSTANCE == null) {
            Dispatcher::$INSTANCE = ClientDispatcher::initFromConfigFile(__DIR__."/../config.json");

        }

        return Dispatcher::$INSTANCE;
    }
}