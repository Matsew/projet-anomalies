<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TicketsController extends AbstractController
{
    /**
     * @Route("/tickets")
     */
    public function tickets(): Response
    {
        return $this->render('consultation_tickets.html.twig', [
            "tickets"=>self::getTickets()
        ]);
    }

    public function getTickets()
    {
        // This function retrieve all the necessary tickets 

        $tickets = [
            [
            "date" => "01/01/2022",
            "name" => "A_Info22-06_Clavier",
            "statut" => "En suspens"
            ],
            [
            "date" => "14/01/2022",
            "name" => "A_Info24-04_Tour",
            "statut" => "Traité"
            ],
            [
            "date" => "17/01/2022",
            "name" => "A_Info24-12_Tour",
            "statut" => "En cours"
            ],
            [
            "date" => "17/01/2022",
            "name" => "A_Info24-12_Tour",
            "statut" => "En cours"
            ],
            [
            "date" => "17/01/2022",
            "name" => "A_Info24-12_Tour",
            "statut" => "En cours"
            ],
            [
            "date" => "17/01/2022",
            "name" => "A_Info24-12_Tour",
            "statut" => "En cours"
            ],
            [
            "date" => "17/01/2022",
            "name" => "A_Info24-12_Tour",
            "statut" => "En cours"
            ]
        ];

       
        return $tickets;

         // - Return an empty list for testing purposes
        return [];
    }

}