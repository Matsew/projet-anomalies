<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TicketController extends AbstractController
{
    /**
     * @Route("/ticket")
     */
    public function ticket(): Response
    {
        
        $name = "A_Info24-04_Tour";
        $date = "14/01/2022";
        $statut = "Traité";
        $source = "Tour";
        $anomalie = "Ne s'allume pas";
        $description = "Relou un peu quand même";
            
        return $this->render('consultation_ticket.html.twig', [
            "name" => $name,
            "date" => $date,
            "statut" => $statut,
            "source" => $source,
            "anomalie" => $anomalie,
            "description" => $description
        ]);
    }

}