<?php

namespace App\Controller;

use App\Dispatcher;

use Exception;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\{Annotation\Route};
use Symfony\Component\HttpFoundation\Response;

use Psr\Log\LoggerInterface;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/buildings")
     * @noinspection PhpUnused
     */
    // TODO PAGINATION ?
    public function getBuildings(LoggerInterface $logger): Response
    {
        try {
            $dispatcher = Dispatcher::getInstance();

            $locationSearchOptions = $dispatcher->getSearchOptions("sidev", "Location");
            $nameId = $locationSearchOptions->getFieldForName("name");

            $locations = $dispatcher->searchItems("sidev", "Location", [
                "criteria" =>
                    [[
                        "field" => $nameId,
                        "searchtype" => 'contains',
                        "value" => '(Batiment)'
                    ]],
                "withindexes" => true
            ]);

            if ($locations->count > 0) {
                $res = array();

                foreach ($locations->data as $id => $loc) {
                    $l = (array)$loc;
                    $item = [
                        "id" => $id,
                        "name" => $l[$nameId],
                        "completename" => $l[$locationSearchOptions->getFieldForName("completename")]
                    ];
                    $res[] = $item;
                }

                return $this->json(
                    $res
                );
            } else {
                return $this->json([]);
            }
        } catch (Exception $e) {
            $logger->error($e->getMessage());
            $response = new Response();
            $response->setStatusCode(500);
            return $response;
        }
    }

    /**
     * @Route("/api/rooms/{buildingId}")
     * @noinspection PhpUnused
     */
    // TODO PAGINATION ?
    public function getRooms(LoggerInterface $logger, string $buildingId)
    {
        try {
            $dispatcher = Dispatcher::getInstance();
            $locationSearchOptions = $dispatcher->getSearchOptions("sidev", "Location");

            $nameId = $locationSearchOptions->getFieldForName("name");

            $locations = $dispatcher->searchItems("sidev", "Location", [
                "criteria" =>
                    [[
                        "field" => "13",
                        "searchtype" => 'equals',
                        "value" => $buildingId
                    ]],
                "forcedisplay" => $nameId,
                "withindexes" => true,
                "giveItems" => true
            ]);

            if ($locations->count > 0) {
                $res = array();

                foreach ($locations->data as $id => $loc) {
                    $l = (array)$loc;

                    $item = [
                        "id" => $id,
                        "name" => $l[$nameId],
                        "completename" => $l[$locationSearchOptions->getFieldForName("completename")],
                    ];
                    $res[] = $item;
                }

                return $this->json(
                    $res
                );
            } else {
                return $this->json([]);
            }
        } catch (Exception $e) {
            $logger->error($e->getMessage());
            $response = new Response();
            $response->setStatusCode(500);
            return $response;
        }
    }


    /**
     * @Route("/api/assets/{locationId}")
     * @noinspection PhpUnused
     */
    // TODO PAGINATION ?
    public function getAssets(LoggerInterface $logger, string $locationId): Response
    {
        try {
            $dispatcher = Dispatcher::getInstance();

            $res = array();

            $computerSearchOptions = $dispatcher->getSearchOptions("sidev", "Computer");
            $assets = $dispatcher->getAssetsInLocation("sidev", $locationId, $computerSearchOptions);

            if ($assets->count > 0) {
                foreach ($assets->data as $asset) {
                    $a = (array)$asset;

                    $item = [
                        "id" => $asset->id,
                        "name" => $a[$computerSearchOptions->getFieldForName("name")],
                        "type" => $asset->itemtype
                    ];
                    $res[] = $item;
                }

                return $this->json(
                    $res
                );
            } else {
                return $this->json([]);
            }
        } catch (Exception $e) {
            $logger->error($e);
            $response = new Response();
            $response->setStatusCode(500);
            return $response;
        }
    }


    /**
     * @Route("/api/location/name/{locationId}")
     * @noinspection PhpUnused
     */
    // TODO PAGINATION ?
    public function getLocationName(LoggerInterface $logger, string $locationId): Response
    {
        try {
            $dispatcher = Dispatcher::getInstance();

            $loc = $dispatcher->getItemHandler("sidev")->getItem("Location", $locationId);
            $loc = json_decode($loc["body"]);
            
            return $this->json([
                "name" => $loc->name,
            ]);
        } catch (Exception $e) {
            $logger->error($e);
            $response = new Response();
            $response->setStatusCode(500);
            return $response;
        }
    }
}