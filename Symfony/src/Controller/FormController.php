<?php

namespace App\Controller;

use App\Dispatcher;
use App\Entity\Ticket;
use Exception;
use ProjetAnomalies\GlpiApi\Exceptions\RequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\Type\TicketType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class FormController extends AbstractController
{

    /**
     * @Route("/anomalie")
     */
    public function anomalie(Request $request): Response
    {
        //isQRCode
        $qrCode = false;
        $departFix = false;
        $salleFix = false;
        $typeFix = false;
        $nomFix = false;
        $depart = "";
        $salle = "";
        $type = "";
        $nom = "";


        if (isset($_GET["departement"])) {
            $departFix = true;
            $dep = $_GET["departement"];
        }
        if (isset($_GET["salle"])) {
            $salleFix = true;
            $salle = $_GET["salle"];
        }
        if (isset($_GET["type"])) {
            $typeFix = true;
            $type = $_GET["type"];
        }
        if (isset($_GET["nom"])) {
            $nomFix = true;
            $nom = $_GET["nom"];
        }

        if (isset($_GET["departement"]) and isset($_GET["salle"]) and isset($_GET["type"]) and isset($_GET["nom"])) {
            $qrCode = true;
        }


        $sourceOrdi = [
            array(
                "name" => "Clavier",
                "reason" => [
                    "cable abimer",
                    "les touche ne reponde pas",
                    "pas reconus par l ordinateur",
                ]
            ),
            array(
                "name" => "Sourie",
                "reason" => [
                    "cable abimer",
                    "le curseur ne bouge pas",
                    "pas reconus par l ordinateur",
                ]
            ),
            array(
                "name" => "Ecran",
                "reason" => [
                    "cable abimer",
                    "ecran ne sallume plus",
                    "bande etrange sur l ecran",
                ]
            ),
        ];

        $sourceAutre = [
            "déteriorer mais fonctionne encore",
            "déteriorer",
        ];

        return $this->render("form.html.twig", [
            "Liste_source_autre" => $sourceAutre,
            "Liste_source_ordi" => $sourceOrdi,
            "departFix" => $departFix,
            "salleFix" => $salleFix,
            "typeFix" => $typeFix,
            "nomFix" => $nomFix,
            "QRCode" => $qrCode,
            "depart" => $depart,
            "salle" => $salle,
            "type" => $type,
            "nom" => $nom,
        ]);
    }

    /**
     * @Route("anomalie/validation")
     */
    public function validate(): Response
    {
        $email = "";
        $departement = "";
        $salle = "";
        $type = "";
        $nom = "";
        $source = "";
        $problem = "";
        $description = "";

        $pb = false;

        if (isset($_POST["ticket"])) {
            $ticket = $_POST["ticket"];

            if (!isset($ticket["email"])) {
                $pb = true;
                $email = "pas de valeur";
            } else if (!preg_match_all(<<<'EOT'
/^[a-z]+\.[a-z]+@(?:[a-z]+\.)?univ-orleans\.fr$/
EOT, $ticket["email"])) {
                $pb = true;
                $email = "L'email est invalid";
            } else {
                $email = $ticket["email"];
            }

            if (!(isset($ticket["departement"]) && $ticket["departement"] != "-- choisissez --")) {
                $pb = true;
                $departement = "pas de valeur";
            } else {
                $departement = $ticket["departement"];
            }

            if (!(isset($ticket["salle"]) && $ticket["salle"] != "-- choisissez --")) {
                $pb = true;
                $salle = "pas de valeur";
            } else {
                $salle = $ticket["salle"];
            }

            if (!(isset($ticket["type"]) && $ticket["type"] != "-- choisissez --")) {
                $pb = true;
                $type = "pas de valeur";
            } else {
                $type = $ticket["type"];
            }

            if (!(isset($ticket["nom"]) && $ticket["nom"] != "-- choisissez --")) {
                $pb = true;
                $nom = "pas de valeur";
            } else {
                $nom = $ticket["nom"];
            }

            if (!(isset($ticket["source"]) && $ticket["source"] != "-- choisissez --")) {
                $source = "pas de valeur";
            } else {
                $source = $ticket["source"];
            }

            if (!(isset($ticket["problem"]) && $ticket["problem"] != "-- choisissez --")) {
                $pb = true;
                $problem = "pas de valeur";
            } else {
                $problem = $ticket["problem"];
            }

            if (!(isset($ticket["description"]) && $ticket["description"] != "-- choisissez --")) {
                $description = "pas de valeur";
            } else {
                $description = $ticket["description"];
            }

            if (!$pb) {
                try {
                    $dispatcher = Dispatcher::getInstance();

                    $handler = $dispatcher->getItemHandler("sidev");

                    $loc = $handler->getItem("Location", $ticket["salle"]);
                    $loc = json_decode($loc["body"]);

                    $computer = $handler->getItem($type, $ticket["nom"]); // TODO TYPE
                    $computer = json_decode($computer["body"]);

                    $nom = $computer->name;
                    $salle = $loc->completename;

                    $content = "<h2>Metadonnées</h2><p>Localisation : " . $loc->completename . "</p><p>Concerne : " . ($ticket["source"] ?? "") . " (" . $ticket["problem"] . ")</p>";

                    if (isset($ticket["description"]) && $ticket["description"] != "") {
                        $content .= "<h2>Description</h2><pre>" . $ticket["description"] . "</pre>";
                    }

                    $res = $dispatcher->addTicket("sidev", [
                        "name" => (($source == "") ? ("Probleme de " . $source . " en ") : "") . $loc->name . " sur " . $computer->name,
                        "content" => $content,
                        "status" => 1,
                        "urgency" => 1,
                        "_users_id_requester" => 0,
                        "locations_id" => $ticket["salle"],
                        "itilcategories_id" => 22,
                        // 'entities_id' => $ticket["nom"],
                        "_users_id_requester_notif" =>
                            [
                                "use_notification" => 1,
                                "alternative_email" => [
                                    $email
                                ]
                            ]
                    ]);

                    return $this->render("validate.html.twig", [
                        "email" => $email,
                        "departement" => $departement,
                        "salle" => $salle,
                        "type" => $type,
                        "nom" => $nom,
                        "source" => $source,
                        "problem" => $problem,
                        "description" => $description,
                    ]);
                } catch (Exception $e) {
                    dump($e);
                    die;
                }
            }
        }

        if ($pb) {
            return $this->render("invalid_validation.html.twig", [
                "email" => $email,
                "departement" => $departement,
                "salle" => $salle,
                "type" => $type,
                "nom" => $nom,
                "source" => $source,
                "problem" => $problem,
                "description" => $description,
            ]);
        } else {
            die;
        }
    }
}
