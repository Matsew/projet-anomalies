<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FormQRCodeController extends AbstractController
{

    /**
     * @Route("/QRCodeGeneration")
     */
    public function qrCodeGeneration(Request $request): Response
    {

        return $this->render("formGenerationQRCode.html.twig",[
        ]);
    }

    /**
    * @Route("QRCodeGenerationMult")
    */
    public function qrCodeGenerationMult(Request $request): Response
    {
  
        return $this->render("formGenerationQRCodeMult.html.twig",[
        ]);
    }
  }
