<?php

namespace App\Form\Type;

use App\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add("email", TextType::class)
            ->add("departement", ChoiceType::class, [
                "choices" => [
                    "None" => "none",
                ]
            ])
            ->add("salle", ChoiceType::class, [
                "choices" => [
                    "None" => "none",
                ]
            ])
            ->add("types", ChoiceType::class, [
                "choices" => [
                    "None" => "none",
                ]
            ])
            ->add("nom", ChoiceType::class, [
                "choices" => [
                    "None" => "none",
                ]
            ])
            ->add("source", ChoiceType::class, [
                "choices" => [
                    "None" => "none",
                ]
            ])
            ->add("problem", ChoiceType::class, [
                "choices" => [
                    "None" => "none",
                ]
            ])
            ->add("description", TextareaType::class)
            ->add("picture", FileType::class, [
                "label" => "Photo",
                "mapped" => false,
                "required" => false,
                "constraints" => [
                    new File([
                        "maxSize" => "4M",
                        "mimeTypes" => [
                            "application/png",
                            "application/jpg",
                            "application/jpeg",
                        ],
                        "mimeTypesMessage" => "Veuillez joindre une image PNG ou JPEG",
                    ])
                ],
            ])
            ->add("submit", SubmitType::class, ["label" => "Valider"]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            "data_class" => Ticket::class,
        ]);
    }
}

