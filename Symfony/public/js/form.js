class Source {
    constructor(type) {
        this._type = type;
        this._ListReason = [];
    }

    toString() {
        return '${this._type}';
    }

    addReason(val) {
        this._ListReason.push(val);
    }

    get reason() {
        return this._ListReason;
    }

    get type() {
        return this._type;
    }
}


class Option {
    constructor(name, value) {
        this._values = value;
        this._name = name;
    }

    toString() {
        return '${this._name}';
    }

    addSalle(val) {
        this._listObject.push(val);
    }

    get value() {
        return this._values;
    }

    get name() {
        return this._name;
    }

}

const list_source_ordi = [];
const list_source_autre = [];

class AppForm {
    constructor() {
        this._ticketEmail = document.getElementById('ticket_email');
        this._ticketDepartement = document.getElementById('ticket_departement');
        this._ticketSalle = document.getElementById('ticket_salle');
        this._ticketType = document.getElementById('ticket_type');
        this._ticketNom = document.getElementById('ticket_nom');
        this._ticketSource = document.getElementById('ticket_source');
        this._ticketProblem = document.getElementById("ticket_problem");

        const self = this;

        this._ticketSource.onchange = (e) => self.updateInfoAnomalieOrdi(e);
        this._ticketDepartement.onchange = (e) => self.updateSalle(e);
        this._ticketSalle.onchange = () => self.updateType();
        this._ticketType.onchange = (e) => self.updateNom(e);
        this._ticketEmail.onchange = (e) => self.checkEmail(e);
    }

    async load() {
        //fil departement
        let optionDepartement = []
        let departement = await GlpiApi.loadBuildingsList();
        for (let i = 0; i < departement.length; i++) {
            optionDepartement.push(new Option(departement[i].name, departement[i].idDep));
        }
        this.fillSelect(this._ticketDepartement, optionDepartement);
        this.bindSourceAnomalie();

        this.resetSalle();
        this.resetType();
        this.resetNom();
        this.resetProblem();
    }

    /**
     * Remplis un select avec la liste des option du select la value.
     *
     * @param {HTMLElement} element
     * @param {*[]} list
     */
    fillSelect(element, list) {
        element.innerHTML = '';
        let option = document.createElement("option");

        option.text = "-- choisissez --";
        element.appendChild(option);

        for (let i = 0; i < list.length; i++) {
            option = document.createElement("option");

            const value = list[i].value;
            const name = list[i].name;

            option.setAttribute("value", value);
            option.text = name;
            element.appendChild(option);
        }
    }

    bindSourceAnomalie() {
        const type = this._ticketType.value;
        if ("Computer" === type) {
            document.getElementById("sectionBinding").style.display = "block";
            return true;
        } else {
            document.getElementById("sectionBinding").style.display = "none";
            return false;
        }
    }

    resetSalle() {
        if(!salleFix){
            this.fillSelect(this._ticketSalle, this.transformListInOption([]));
        }
    }

    resetType() {
        if(!typeFix){
            this.fillSelect(this._ticketType, this.transformListInOption([]));
            this.bindSourceAnomalie(source);
        }
    }

    resetNom() {
        if(!nomFix){
            this.fillSelect(this._ticketNom, this.transformListInOption([]));
        }
    }

    resetProblem() {
        this.fillSelect(this._ticketProblem, this.transformListInOption([]));
    }


    checkEmail(element) {
        let enteredEmail = element.target.value;
        const mail_format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        let spanMail = document.getElementById("emailHelp");
        if (enteredEmail.match(mail_format)) {
            if (enteredEmail.endsWith('univ-orleans.fr')) {
                spanMail.className = "text-success";
                spanMail.innerText = "adresse mail valide";
                return true;
            }
            spanMail.className = "text-danger";
            spanMail.innerText = "attention l'adresse mail ne fini pas par univ-orleans.fr";
            return false;
        }
        spanMail.className = "text-danger";
        spanMail.innerText = "attention ce n'est pas une adresse mail non valide il faut un @ et qu'elle finissent par univ-orleans.fr";
        return false;
    }


    async updateSalle(element) {
        if(!salleFix){
            console.log("update Salle");
            let source = element.target.value;
            let optionSalle = []
            let salle = await GlpiApi.loadRoomList(source);
            for (let i = 0; i < salle.length; i++) {
                optionSalle.push(new Option(salle[i].name, salle[i].idSalle));
            }

            this.fillSelect(this._ticketSalle, optionSalle);
            this.resetType();
            this.resetNom();
            this.resetProblem();
            this.bindSourceAnomalie(source);
        }
    }

    async updateType() {
        if(!typeFix){
            console.log("update Type");
            let optionType = []
            let option = []
            let source = document.getElementById("ticket_salle").value;
            let objet = await GlpiApi.loadAssetsList(source);
            for (let i = 0; i < objet.length; i++) {
                if (option.indexOf(objet[i].type) !== -1) {

                } else {

                    option.push(objet[i].type)
                    optionType.push(new Option(objet[i].type, objet[i].type));
                }
            }
            this.fillSelect(this._ticketType, optionType);
            this.updateSourceAnomalie();
            this.resetNom();
            this.resetProblem();
            this.bindSourceAnomalie(source);
        }
    }


    async updateNom(element) {
        if(!nomFix){
            console.log("update Nom");
            let type = element.target.value;
            let source = document.getElementById("ticket_salle").value;
            let optionNom = []
            let nom = await GlpiApi.loadAssetsList(source);
            for (let i = 0; i < nom.length; i++) {
                if (nom[i].type === type) {
                    optionNom.push(new Option(nom[i].name, nom[i].idObj));
                }
            }

            this.fillSelect(this._ticketNom, optionNom);
            this.bindSourceAnomalie(source);
            this.updateInfoAnomalieAutre();
        }
    }


    updateSourceAnomalie() {
        let optionType = [];
        for (const element of list_source_ordi) {
            optionType.push(new Option(element.type, element.type));
        }
        this.fillSelect(this._ticketSource, optionType);
    }


    updateInfoAnomalieOrdi(element) {
        let source = element.target.value;
        for (const e of list_source_ordi) {
            console.log(e);
            if (e.type === source) {
                this.fillSelect(this._ticketProblem, this.transformListInOption(e.reason));
            }
        }
    }


    updateInfoAnomalieAutre() {
        this.fillSelect(this._ticketProblem, this.transformListInOption(list_source_autre));
    }

    transformListInOption(list) {
        let res = [];
        for (const val of list) {
            res.push(new Option(val, val));
        }
        return res;
    }
}

window.onload = async () => {
    const app = new AppForm();

    await app.load();
}
