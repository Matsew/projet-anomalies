console.log('lancement...');

/**
 * 
 * @returns {Object} The dictionnay of the parameters necessary for the QRCode
 */
const getParams = () => {
    /**
     * @return {Array}  The list of params from the form
     */
    let params = {
        "departement" : "",
        "salle" : "",
        "type" : "",
        "nom" : ""
      };
    params["departement"] = document.getElementById('select-departement').value;
    params["salle"] = document.getElementById('select-salle').value;
    
    let p = document.getElementById('select-name');
    params["type"] = p[p.selectedIndex].id;
    params["nom"] = p.value;
    

    return params;
}

/**
 * Generate and display the QRCode.
 */
 async function makeCode() {
    // Get the value from the fields
    const params = getParams();
    const divQRCode = document.getElementById('qrcode');

    // Remove all child if present
    if (divQRCode.hasChildNodes()) { 
        divQRCode.innerHTML = '';
    }

    // information of qrcode
    ajouterInformations(divQRCode);
    
    // Generation of qrcode
    let qrcode = new QRCode("qrcode");
    qrcode.makeCode(makeLien(params));
    createPrintButton(divQRCode);
}

/**
 * Add the information corresponding to the QRCode.
 * @param  {List} params  The list of the parameters
 * @param  {Node} divQRCode  The node aimed to contain the information
 */ 
function ajouterInformations(divQRCode) {

    // Retrieve informations
    let departementSelect = document.getElementById('select-departement');
    let salleSelect = document.getElementById('select-salle');
    let materielSelect = document.getElementById('select-name');

    let departement = departementSelect[departementSelect.selectedIndex].text;
    let salle = salleSelect[salleSelect.selectedIndex].text;
    let materiel = materielSelect[materielSelect.selectedIndex].text;
    

    // Creation of paragraph section

    let pDep = document.createElement("p");
    let pSalle = document.createElement("p");
    let pNom = document.createElement("p");

    // Add text to paragraph 
    pDep.append("Departement : " + departement);
    pSalle.append("Salle : " + salle);
    pNom.append("Nom : " + materiel);

    // Add to the page
    let container = document.createElement('div');
    container.id = "informations";
    container.appendChild(pDep);
    container.appendChild(pSalle);
    container.appendChild(pNom);
    divQRCode.append(container);
}

/**
 * Generate and display the QRCode.
 * @param  {List} params  The list of the parameters
 */   
function makeAllCodes(params) {
    
    for (let i=0; i<10; ++i) {
        let qrcode = new QRCode("qrcode");
        qrcode.makeCode(makeLien(params));
    }

    // HELP : Mettre le QR code au centre

    const information = document.getElementById('container-information-content');
    console.log(information)

}

/**
 * Create the string -> link corresponding to the QRCode.
 * @param  {List}    params  The list of the parameters
 * @return {String}          The link of the QRCode.
 */
function makeLien(params) {
    let lien = "http://127.0.0.1:8000/anomalie?departement=" ;
    lien += params["departement"];
    lien += "&salle=";
    lien += params["salle"];
    lien += "&type=";
    lien += params["type"];
    lien += "&nom="
    lien += params["nom"];
    return lien;
}


/**
 * Create the print button and append it into the node.
 * @param  {List}    node  The node aimed to contain the print button
 */
const createPrintButton = (node) => {
    let btn = document.createElement('button');
    btn.id = "btn-print";
    btn.classList.add("btn", "btn-primary");
    btn.onclick = () =>{
        printQR('qrcode');
    };
    btn.append('Imprimer');
    node.appendChild(btn);
}

/**
 * Make the corresponding button displayed on the screen (by removing bootstrap class)
 * @param  {String} id  The id of the button to make visible
 */
const displayBtn = (id) => {
    document.getElementById(id).classList.remove('d-none')
}
