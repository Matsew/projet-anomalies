"use-strict"

console.log('Script has launched');

/**
 * 
 * @param {String} source the name of the element to show next
 * @param {Node} element the select Node to retrive its value
 */
function onChangeSelectDep(source, element){
   
   // Retrieve the selected value
   let value = element.value;

   // Display the next select field
   show(document.getElementById(`container-select-${source}`));

   // Built the label for next select field
   document.getElementById(`label-${source}`).innerText = `Séléctionnez une salle`;

   fillSelectRooms(value);

}

function onChangeSelectRoom(source, element){
   
   // Retrieve the selected value
   let value = element.value;

   // Display the next select field
   show(document.getElementById(`container-select-${source}`));

   // Built the label for next select field
   document.getElementById(`label-${source}`).innerText = `Séléctionnez un materiel`;

   fillSelectAsset(value);
   
}



/**
 * Print the QRCode
 * @param {Node} node The node aimed to contais the part of the document to be printed 
 */
const printQR = (div) => {
   let prtContent = document.getElementById(div);
   let WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
   WinPrint.document.write(prtContent.innerHTML);
   WinPrint.document.close();
   WinPrint.focus();
   WinPrint.print();
   WinPrint.close();
}
