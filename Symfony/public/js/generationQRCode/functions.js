"use-strict"
console.log('utilities function loaded');

/**
 * Make a node visible (displayed on screen by removing d-none bootstrap class)
 * @param {Node} node the Node to make visible 
 */
const show = (node) => {
    node.classList.remove("d-none");
}

/**
 * Make a node invisible (removed from screen by adding d-none bootstrap class)
 * @param {Node} node the Node to make non visible 
 */
 const hide = (node) => {
    node.classList.add("d-none");
}


const fillSelectBuildings = async () => {
    let node = document.getElementById('select-departement');
    let list = await GlpiApi.loadBuildingsList(); 

    node.innerHTML = '';

    let option = new Option("Choose here", "", defaultSelected=true, selected=true); // en placeholder

    node.appendChild(option);

    for (let i = 0; i < list.length; i++) {
        let option = new Option(list[i].name, list[i]._idSalle);
        node.appendChild(option);
    }

}

const fillSelectRooms = async () => {
    let id = document.getElementById('select-departement').value;
    console.log(id);
    let node = document.getElementById('select-salle');
    let list = await GlpiApi.loadRoomList(id);

    node.innerHTML = '';

    let option = new Option("Choose here", "", defaultSelected=true, selected=true); // en placeholder
    node.appendChild(option);

    for (let i = 0; i < list.length; i++) {
        let option = new Option(list[i].name, list[i]._idSalle);
        node.appendChild(option);
    }
}


const fillSelectAsset = async () => {
    let id = document.getElementById('select-salle').value;
    let node = document.getElementById('select-name');
    let list = await GlpiApi.loadAssetsList(id);

    node.innerHTML = '';

    let option = new Option("Choose here", "", defaultSelected=true, selected=true); // en placeholder
    node.appendChild(option);

    for (let i = 0; i < list.length; i++) {
        let option = new Option(list[i].name, list[i]._idObject);
        option.id = list[i].type;
        node.appendChild(option);
    }
}



fillSelectBuildings();







// NE FONCTIONNE PAS - TROUVER UNE SOLUTION
const test = async () => {
    let departement = await GlpiApi.loadBuildingsList();
}