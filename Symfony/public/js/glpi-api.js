/**
 * An interface to GLPI.
 */
class GlpiApi {
    /**
     * Fetch the list of buildings.
     *
     * @returns {Promise<BuildingGLPI[]>} The list of buildings.
     */
    static async loadBuildingsList() {
        const res = await fetch("/api/buildings")
            .then(r => r.json());

        let listBuildings = [];
        for (const buildings of res) {
            listBuildings.push(new BuildingGLPI(buildings.id, buildings.name.slice(0, -11), buildings.completename.slice(0, -11)));
        }

        return listBuildings;
    }

    /**
     * Fetch the list of rooms in a building.
     *
     * @param {string} idBuilding The building id, retrieved with loadBuildingsList.
     * @returns {Promise<RoomGLPI[]>} The list of rooms in the building.
     */
    static async loadRoomList(idBuilding) {
        let res = await fetch("/api/rooms/" + idBuilding)
            .then(r => r.json());

        let listRooms = [];
        for (const room of res) {
            listRooms.push(new RoomGLPI(room.id, room.name, room.completename));
        }

        return listRooms;
    }

    /**
     * Fetch the list of assets in a room.
     *
     * @param {string} idRoom The room id, retrieved with loadRoomList.
     * @returns {Promise<AssetGLPI[]>} The list of assets in the room.
     */
    static async loadAssetsList(idRoom) {
        let res = await fetch("/api/assets/" + idRoom)
            .then(r => r.json());

        let listAssets = [];
        for (const asset of res) {
            listAssets.push(new AssetGLPI(asset.id, asset.name, asset.type));
        }

        return listAssets;
    }

}

/**
 * An asset (Computer, Monitor ...).
 */
class AssetGLPI {
    /**
     * @param {string} id The id of the asset.
     * @param {string} name The name of the asset.
     * @param {string} type The type of the asset.
     */
    constructor(id, name, type) {
        this._idObject = id;
        this._name = name;
        this._type = type;
    }

    toString() {
        return '${this._name}' + '${this._type}';
    }

    /**
     *
     * @returns {string}
     */
    get idObj() {
        return this._idObject;
    }

    /**
     *
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     *
     * @returns {string}
     */
    get type() {
        return this._type;
    }
}

class RoomGLPI {
    /**
     *
     * @param {string} id The id of the room.
     * @param {string} name The name of the room.
     * @param {string} completename The complete name (/(Parents > )*Name/).
     */
    constructor(id, name, completename) {
        this._idSalle = id;
        this._name = name;
        this._completename = completename;
        this._listAssets = [];
    }

    toString() {
        return '${this._name}';
    }

    /**
     *
     * @param {AssetGLPI} val
     */
    addObject(val) {
        this._listAssets.push(val);
    }

    /**
     *
     * @returns {string}
     */
    get idSalle() {
        return this._idSalle;
    }

    /**
     *
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     *
     * @returns {string}
     */
    get completename() {
        return this._completename;
    }

    /**
     *
     * @returns {AssetGLPI[]}
     */
    get listAssets() {
        return this._listAssets;
    }
}

class BuildingGLPI {
    /**
     *
     * @param {string} id
     * @param {string} name
     * @param {string} completename
     */
    constructor(id, name, completename) {
        this._idSalle = id;
        this._name = name;
        this._completename = completename;
        this._listRooms = [];
    }

    /**
     *
     * @returns {string}
     */
    toString() {
        return '${this._name}';
    }

    /**
     *
     * @param {RoomGLPI} val
     */
    addRoom(val) {
        this._listRooms.push(val);
    }

    /**
     *
     * @returns {string}
     */
    get idDep() {
        return this._idSalle;
    }

    /**
     *
     * @returns {string}
     */
    get name() {
        return this._name;
    }

    /**
     *
     * @returns {string}
     */
    get completename() {
        return this._completename;
    }

    /**
     *
     * @returns {RoomGLPI[]}
     */
    get listRoom() {
        return this._listRooms;
    }
}