<?php

namespace ProjetAnomalies\GlpiApi\Exceptions;

use Exception;
use Throwable;

class RequestException extends Exception
{
    private int $statusCode;
    private string $errorCode;
    private string $errorDescription;

    public function __construct(string $errorDescription, string $errorCode, int $statusCode, $code = 0, Throwable $previous = null)
    {
        $this->statusCode = $statusCode;
        $this->errorCode = $errorCode;
        $this->errorDescription = $errorDescription;
        parent::__construct($errorDescription, $code, $previous);
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }
}