<?php
declare(strict_types=1);

namespace ProjetAnomalies\GlpiApi;

use Exception;
use phpDocumentor\Reflection\Location;
use stdClass;

use Glpi\Api\Rest\Client;
use Glpi\Api\Rest\ItemHandler;
use ProjetAnomalies\GlpiApi\Exceptions\RequestException;

class ClientDispatcher
{
    private array $handlers;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->handlers = [];
    }

    /**
     * @throws Exception
     */
    public static function initFromConfigFile(string $path): ClientDispatcher
    {
        $file_content = file_get_contents($path) or die("file not found");
        $config = json_decode($file_content);
        $dispatcher = new ClientDispatcher();

        foreach ($config->api_client as $client_config) {
            $client = new Client($client_config->api_url, new \GuzzleHttp\Client());
            $client->setAppToken($client_config->app_token);
            $client->initSessionByUserToken($client_config->user_token);

            $dispatcher->handlers[$client_config->id] = new ItemHandler($client);
        }

        return $dispatcher;
    }

    /**
     * @throws RequestException
     */
    public function getTicket(string $instance, int $id, array $queryString = []): StdClass
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->getItem("Ticket", $id, $queryString);

        if ($res["statusCode"] == 200) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * @throws RequestException
     */
    public function getTickets(string $instance, array $queryString = []): array
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->getAllItems("Ticket", $queryString);

        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            var_dump($content);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * @throws RequestException
     */
    public function addTicket(string $instance, array $params): StdClass
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->addItems("Ticket", $params);
        if ($res["statusCode"] == 201) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * @throws RequestException
     */
    public function updateTicket(string $instance, int $id, array $params = []): array
    {
        $itemHandler = $this->handlers[$instance];
        $res = $itemHandler->updateItems("Ticket", $id, $params);
        if ($res["statusCode"] == 200) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * @return Client[]
     */
    public function getHandlers(): array
    {
        return $this->handlers;
    }

    /**
     * @param string $instance The instance id.
     * @return Client The client bound to this instance.
     */
    public function getClient(string $instance): Client
    {
        return $this->handlers[$instance]->getClient();
    }

    /**
     * @param string $instance The instance id.
     * @return ItemHandler
     */
    public function getItemHandler(string $instance): ItemHandler
    {
        return $this->handlers[$instance];
    }

    /**
     * @throws RequestException
     */
    public function getSearchOptions(string $instance, string $itemType): SearchOptions
    {
        return SearchOptions::getForItemType($this->handlers[$instance], $itemType);
    }

    /**
     * @throws RequestException
     */
    public function searchItems(string $instance, string $itemType, array $queryString): StdClass
    {
        $res = $this->handlers[$instance]->searchItems($itemType, $queryString);
        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * @throws RequestException
     */
    public function getCategories(string $instance, array $queryString = []): array
    {
        $res = $this->handlers[$instance]->getAllItems("ITILCategory", $queryString);

        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }

    /**
     * @throws RequestException
     */
    public function getLocations(string $instance, array $queryString = []): array
    {
        $res = $this->handlers[$instance]->getAllItems("Location", $queryString);

        if ($res["statusCode"] == 200 || $res["statusCode"] == 206) {
            return json_decode($res["body"]);
        } else {
            $content = json_decode($res["body"]);
            throw new RequestException($content[1], $content[0], $res["statusCode"]);
        }
    }


    /**
     * @throws RequestException
     */
    public function getLocationId(string $instance, string $locationName, ?SearchOptions $locationSearchOptions = null): ?string
    {
        if ($locationSearchOptions == null) {
            $locationSearchOptions = $this->getSearchOptions($instance, "Location");
        }

        $nameFieldId = $locationSearchOptions->getFieldForName("name");

        $res = $this->searchItems("sidev", "Location", [
            "criteria" => [[
                "field" => $nameFieldId,
                "searchtype" => "contains",
                "value" => $locationName // "I02"
            ]],
            "withindexes" => true
        ]);


        foreach ($res->data as $id => $value) {
            if (((array)$value)[$nameFieldId] == $locationName ) {
                return $id;
            }

        }

        return null;
    }

    /**
     * @throws RequestException
     */
    public function getAssetsInLocation(string $instance, string $locationId, ?SearchOptions $computerSearchOptions = null): StdClass
    {
        if ($computerSearchOptions == null) {
            $computerSearchOptions = $this->getSearchOptions($instance, "Computer");
        }

        return $this->searchItems("sidev", "AllAssets", [
            "criteria" =>
                [[
                    "field" => $computerSearchOptions->getFieldForName("completename"),
                    "searchtype" => 'equals',
                    "value" => $locationId
                ]],
        ]);
    }
}