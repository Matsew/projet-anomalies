<?php

namespace ProjetAnomalies\GlpiApi;

use Glpi\Api\Rest\ItemHandler;
use ProjetAnomalies\GlpiApi\Exceptions\RequestException;
use StdClass;

class SearchOptions
{
    private StdClass $searchOptions;

    /**
     * @throws RequestException
     */
    public static function getForItemType(ItemHandler $itemHandler, string $itemType): SearchOptions
    {
        $requestRes = $itemHandler->listSearchOptions($itemType);
        if ($requestRes["statusCode"] == 200) {
            $searchOptions = json_decode($requestRes["body"]);
            $res = new SearchOptions();

            $res->searchOptions = $searchOptions;

            return $res;
        } else {
            $content = json_decode($requestRes["body"]);
            throw new RequestException($content[1], $content[0], $requestRes["statusCode"]);
        }
    }

    public function getFieldForName(string $name): ?string
    {
        foreach ($this->searchOptions as $key => $option) {
            if (gettype($option) == "object" && property_exists($option, "field") && $option->field == $name) {
                return $key;
            }
        }

        return null;
    }

    public function getFieldForUID(string $uid): ?string
    {
        foreach ($this->searchOptions as $key => $option) {
            if (gettype($option) == "object" && property_exists($option, "uid") && $option->uid == $uid) {
                return $key;
            }
        }

        return null;
    }

    public function getField(string $fieldId): StdClass
    {
        return $this->searchOptions[$fieldId];
    }
}